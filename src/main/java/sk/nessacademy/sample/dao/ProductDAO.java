package sk.nessacademy.sample.dao;

import java.util.List;

import sk.nessacademy.sample.domain.Product;

public interface ProductDAO {

  /** Removes {@link Product} into the DB */
  void persist(Product product);

  /** Removes provided {@link Product} */
  void delete(Product product);

  /** Finds {@link Product} with provided ID */
  Product findById(Integer productId);

  /** Finds {@link Product} with provided name */
  Product findByName(String productName);

  /** Finds all products using HQL */
  List<Product> findAll();

  /** Finds all products using SQL */
  List<Product> findAllWithSQL();

  /** Finds all products. Handling transactions manually */
  List<Product> findAllNoSpringTransactionMng();

}

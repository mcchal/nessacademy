package sk.nessacademy.sample.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import sk.nessacademy.sample.dao.ProductDAO;
import sk.nessacademy.sample.domain.Product;

/**
 * DAO for {@link Product} related DB operations
 *
 * @author michal.kmetka
 *
 */
@Repository("productDAO")
public class ProductHibernateDAO implements ProductDAO {

  @Resource(name = "sessionFactory")
  private SessionFactory sessionFactory;

  @Override
  public void persist(final Product product) {
    this.sessionFactory.getCurrentSession().saveOrUpdate(product);
  }

  @Override
  public void delete(final Product product) {
    this.sessionFactory.getCurrentSession().delete(product);
  }

  @Override
  public Product findById(final Integer productId) {
    return (Product) this.sessionFactory.getCurrentSession().get(Product.class, productId);
  }

  @Override
  public Product findByName(final String productName) {
    return (Product) this.sessionFactory.getCurrentSession().createQuery("from Product p where p.productName = :name").setString("name", productName)
        .uniqueResult();
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Product> findAll() {
    return this.sessionFactory.getCurrentSession().createQuery("from Product").list();
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Product> findAllWithSQL() {
    return this.sessionFactory.getCurrentSession().createSQLQuery("select {p.*} from product p").addEntity("p", Product.class).list();
  }

  @Override
  @SuppressWarnings("unchecked")
  public List<Product> findAllNoSpringTransactionMng() {
    final Session sessions = this.sessionFactory.openSession();

    final Transaction transaction = sessions.beginTransaction();

    final List<Product> products = sessions.createQuery("from Product").list();

    transaction.commit();
    sessions.close();

    return products;
  }

}

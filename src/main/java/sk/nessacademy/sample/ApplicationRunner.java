package sk.nessacademy.sample;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import sk.nessacademy.sample.config.Config;
import sk.nessacademy.sample.service.ProductService;

public class ApplicationRunner {

  private static final Logger LOGGER = LogManager.getLogger(ApplicationRunner.class);

  @SuppressWarnings("resource")
  public static void main(final String[] args) {
    final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);

    final ProductService productService = context.getBean("productService", ProductService.class);
    final ProductService productServiceNonTransactional = context.getBean("productNonTransactionalService", ProductService.class);

    productService.createProduct("product1");
    productService.createProduct("product2");
    productService.createProduct("product3");

    LOGGER.debug(String.format("All current products: %s", productService.getAllProducts()));

    productService.removeProduct(productService.getProductByName("product2").getId());

    LOGGER.debug(String.format("All current products: %s", productServiceNonTransactional.getAllProducts()));

  }

}

package sk.nessacademy.sample.service;

import java.util.List;

import sk.nessacademy.sample.domain.Product;

public interface ProductService {

  /** Creates a new {@link Product} with provided name */
  void createProduct(String productName);

  /** Removes {@link Product} with provided ID */
  void removeProduct(Integer productId);

  /** Returns {@link Product} with provided name. Returns null if product doesn't exists */
  Product getProductByName(String productName);

  /** Returns all available {@link Product}s */
  List<Product> getAllProducts();

}

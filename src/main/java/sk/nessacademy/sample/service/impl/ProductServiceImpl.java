package sk.nessacademy.sample.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sk.nessacademy.sample.dao.ProductDAO;
import sk.nessacademy.sample.domain.Product;
import sk.nessacademy.sample.service.ProductService;

/**
 * Service should be used as a gateway to {@link Product} world and handle all product related manipulation.
 *
 * @author michal.kmetka
 *
 */
@Transactional
@Service("productService")
public class ProductServiceImpl implements ProductService {

  private static final Logger LOGGER = LogManager.getLogger(ProductServiceImpl.class);

  @Resource(name = "productDAO")
  private ProductDAO productDAO;

  @Override
  public void createProduct(final String productName) {
    final Product product = new Product();
    product.setProductName(productName);

    LOGGER.debug(String.format("Creating product: %s", product));

    this.productDAO.persist(product);
  }

  @Override
  public void removeProduct(final Integer productId) {
    final Product product = this.productDAO.findById(productId);

    if (product != null) {
      LOGGER.debug(String.format("Removing product: %s", product));

      this.productDAO.delete(product);
    }
  }

  @Override
  public Product getProductByName(final String productName) {
    return this.productDAO.findByName(productName);
  }

  @Override
  public List<Product> getAllProducts() {
    return this.productDAO.findAll();
  }

}

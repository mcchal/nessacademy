package sk.nessacademy.sample.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Simple spring configurations with component scan and enabling transaction management
 *
 * @author michal.kmetka
 *
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = "sk.nessacademy")
@Import(DatabaseConfig.class)
public class Config {

}

package sk.nessacademy.sample.config;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

public class DatabaseConfig {

  @Bean(name = "sessionFactory")
  public LocalSessionFactoryBean sessionFactory() {
    final org.hibernate.cfg.Configuration configuration = new org.hibernate.cfg.Configuration()
    .setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect")
    .setProperty("hibernate.connection.driver_class", "org.hsqldb.jdbcDriver")
    .setProperty("hibernate.connection.url", "jdbc:hsqldb:file:mydb;shutdown=true")
    .setProperty("hibernate.connection.username", "sa")
    .setProperty("hibernate.connection.password", "")
    .setProperty("hibernate.connection.pool_size", "1")
    .setProperty("hibernate.connection.autocommit", "true")
    .setProperty("hibernate.hbm2ddl.auto", "create")
    .setProperty("hibernate.show_sql", "false");

    final LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
    sessionFactory.setDataSource(dataSource());
    sessionFactory.setPackagesToScan(new String[] { "sk.nessacademy.sample.domain" });
    sessionFactory.setHibernateProperties(configuration.getProperties());

    return sessionFactory;
  }

  @Bean(name = "dataSource")
  public DataSource dataSource() {
    return new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.HSQL).setName("mydb").build();
  }

  @Bean(name = "transactionManager")
  public PlatformTransactionManager transactionManager(final SessionFactory sessionFactory) {
    return new HibernateTransactionManager(sessionFactory);
  }

}
